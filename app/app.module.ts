import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { routing } from './app.routing';

import { AppComponent }   from './app.component';
import { HomeComponent } from './home/components/home.component'
import { EventComponent } from './events/components/events.component';
import { EventRegisterComponent } from './events/components/event-newevent.component';
import { EventThumbnailComponent } from './events/components/event-thumbnail.component';
import { EmployeeComponent } from './employees/employees.component';
import { JsonPostComponent } from './jsonpost/components/json-post.component';
import { EventDetailsComponent } from './events/components/event-details.component';

import { FirstLetterCapitalPipe } from './events/shared/first-letter-capital.pipe';
import { EventFilterPipe } from './events/shared/event-filter.pipe';

import { EventService } from './events/services/events.service.1';
import { JsonPostService } from './jsonpost/services/json-post.service';

@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, routing ],
  declarations: [ AppComponent, HomeComponent, EventComponent, EventDetailsComponent, EventRegisterComponent, EmployeeComponent, EventThumbnailComponent, JsonPostComponent, FirstLetterCapitalPipe, EventFilterPipe ],
  bootstrap:    [ AppComponent ],
  providers:    [ EventService, JsonPostService ]
})

export class AppModule { }