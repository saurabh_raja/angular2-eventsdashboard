import { Injectable } from '@angular/core';

import { Event } from '../models/events.model';

@Injectable()
export class EventService {
    constructor() {}

    private events : Event[] = [
        {
            "eventId": 1,
            "eventName": "Angular 2 - Seminar",
            "eventCode": "NG-SEMI234",
            "releaseDate": "Dec 31, 2016",
            "description": "Angular 2 - Introduction to New features in Angular 2.",
            "fees": 500,
            "popularity": 4.2,
            "imageUrl": "app/events/images/1.jpg"
        },
        {
            "eventId": 2,
            "eventName": "jQuery 3 - Seminar",
            "eventCode": "JQ-SEMI424",
            "releaseDate": "Jan 3, 2017",
            "description": "jQuery 3 - Introduction to New features in jQuery 3.",
            "fees": 300,
            "popularity": 3.2,
            "imageUrl": "app/events/images/2.jpg"
        },
        {
            "eventId": 3,
            "eventName": "Angular 2 - Best Practices",
            "eventCode": "NG-PNP279",
            "releaseDate": "Jan 7, 2017",
            "description": "Angular 2 - Patterns and Practices and how to write best code.",
            "fees": 700,
            "popularity": 4.4,
            "imageUrl": "app/events/images/3.jpg"
        },
        {
            "eventId": 4,
            "eventName": "Angular 2 - Migration",
            "eventCode": "NG-MI377",
            "releaseDate": "Jan 23, 2017",
            "description": "Angular 2 - Migration from Angular JS 1.x to Angular 2.",
            "fees": 900,
            "popularity": 4.9,
            "imageUrl": "app/events/images/4.jpg"
        },
        {
            "eventId": 5,
            "eventName": "Angular 2 - Rapid Fire",
            "eventCode": "NG-RF945",
            "releaseDate": "Jan 30, 2017",
            "description": "Angular 2 - Rapid Fire Session on Angular 2.",
            "fees": 300,
            "popularity": 3.3,
            "imageUrl": "app/events/images/5.jpg"
        }
    ];

    getAllEvents() {
        return this.events;
    }

    getEventDetails(id : number) {
        for(let event of this.events) {
            if(event.eventId == id) {
                return event;
            }
        }
    }

    saveEvent(event : Event) {
        event.eventId = 10;
        console.log("Event");
        console.log(event);
        this.events.push(event);
    }
}
