import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { Event } from '../models/events.model';

@Injectable()
export class EventService {
    constructor(private _http : Http) {}

    private _eventDataUrl = "../../EventsData/events.json";

    getAllEvents() : Observable<Event[]> {
        return this._http.get(this._eventDataUrl)
            .map(res => res.json());    // Fat arrow inline function. json returns stringify data
    }
}
