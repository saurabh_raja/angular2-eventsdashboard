"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var EventService = (function () {
    function EventService() {
        this.events = [
            {
                "eventId": 1,
                "eventName": "Angular 2 - Seminar",
                "eventCode": "NG-SEMI234",
                "releaseDate": "Dec 31, 2016",
                "description": "Angular 2 - Introduction to New features in Angular 2.",
                "fees": 500,
                "popularity": 4.2,
                "imageUrl": "app/events/images/1.jpg"
            },
            {
                "eventId": 2,
                "eventName": "jQuery 3 - Seminar",
                "eventCode": "JQ-SEMI424",
                "releaseDate": "Jan 3, 2017",
                "description": "jQuery 3 - Introduction to New features in jQuery 3.",
                "fees": 300,
                "popularity": 3.2,
                "imageUrl": "app/events/images/2.jpg"
            },
            {
                "eventId": 3,
                "eventName": "Angular 2 - Best Practices",
                "eventCode": "NG-PNP279",
                "releaseDate": "Jan 7, 2017",
                "description": "Angular 2 - Patterns and Practices and how to write best code.",
                "fees": 700,
                "popularity": 4.4,
                "imageUrl": "app/events/images/3.jpg"
            },
            {
                "eventId": 4,
                "eventName": "Angular 2 - Migration",
                "eventCode": "NG-MI377",
                "releaseDate": "Jan 23, 2017",
                "description": "Angular 2 - Migration from Angular JS 1.x to Angular 2.",
                "fees": 900,
                "popularity": 4.9,
                "imageUrl": "app/events/images/4.jpg"
            },
            {
                "eventId": 5,
                "eventName": "Angular 2 - Rapid Fire",
                "eventCode": "NG-RF945",
                "releaseDate": "Jan 30, 2017",
                "description": "Angular 2 - Rapid Fire Session on Angular 2.",
                "fees": 300,
                "popularity": 3.3,
                "imageUrl": "app/events/images/5.jpg"
            }
        ];
    }
    EventService.prototype.getAllEvents = function () {
        return this.events;
    };
    EventService.prototype.getEventDetails = function (id) {
        for (var _i = 0, _a = this.events; _i < _a.length; _i++) {
            var event_1 = _a[_i];
            if (event_1.eventId == id) {
                return event_1;
            }
        }
    };
    EventService.prototype.saveEvent = function (event) {
        event.eventId = 10;
        console.log("Event");
        console.log(event);
        this.events.push(event);
    };
    EventService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], EventService);
    return EventService;
}());
exports.EventService = EventService;
//# sourceMappingURL=events.service.1.js.map