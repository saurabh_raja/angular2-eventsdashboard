import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from '@angular/router';

import { Event } from "../models/events.model";

import { EventService } from '../services/events.service.1';

@Component({
    selector: "event-details",
    templateUrl:"app/events/views/event-details.component.html",
    styles: [
        `
            .bold { font-weight: bold }
            .margin-top-3 { margin-top : 3% }
        `
    ]
})

export class EventDetailsComponent implements OnInit {
    constructor(private _eventService : EventService, private route : ActivatedRoute) {

    }

    title: string;

    eventDetail : Event = <Event>{};

    event : Event;

    imageHeight : number = 100;

    imageWidth : number = 100;

    id : number;

    ngOnInit() {
        this.getSingleEvent();
    }

    getSingleEvent() {
        this.route.params.subscribe(params => {
            this.id = params['id'];
        });

        this.event = this._eventService.getEventDetails(this.id);
        this.title = "Details of Event - " + this.event.eventName;
    }

    getEventNameColor(fees : number) : String {
        let className = "";

        if(fees == 300) {
            className = "cheap";
        } else if(fees == 500) {
            className = "moderate";
        } else if(fees > 500) {
            className = "expensive";
        }

        return className;
    }
}