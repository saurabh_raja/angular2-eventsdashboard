import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';

import { Event } from "../models/events.model";

import { EventService } from '../services/events.service.1';

@Component({
    selector: "event-newevent",
    templateUrl:"app/events/views/event-newevent.component.html",
    styles: [
        `
            .margin-top-1 { margin-top:1% }
            .error input { background-color: #E3C3C5 }
            .font-red { color: red }
        `
    ]
})

export class EventRegisterComponent implements OnInit {
    constructor(private _eventService : EventService, private router : Router) {

    }

    title: string;

    eventId: number;
    eventName: string;
    eventCode: string;
    releaseDate: string;
    description: string;
    fees: number;
    popularity: number;
    imageUrl: string;

    ngOnInit() {
        
    }

    saveNewEvent(event : Event) : void {
        console.log("First Event");
        console.log(event);
        this._eventService.saveEvent(event);
        this.router.navigate(['/events']);
    }
}