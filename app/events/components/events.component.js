"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var events_service_1_1 = require('../services/events.service.1');
var EventComponent = (function () {
    function EventComponent(_eventService) {
        this._eventService = _eventService;
        this.title = "Event List";
        this.photoClickedMessage = "Not checked out";
        this.filterBy = "";
    }
    EventComponent.prototype.ngOnInit = function () {
        this.events = this._eventService.getAllEvents();
    };
    EventComponent.prototype.getEventNameColor = function (fees) {
        var className = "";
        if (fees == 300) {
            className = "cheap";
        }
        else if (fees == 500) {
            className = "moderate";
        }
        else if (fees > 500) {
            className = "expensive";
        }
        return className;
    };
    EventComponent = __decorate([
        core_1.Component({
            selector: "event-list",
            templateUrl: "app/events/views/events.component.html",
            styles: [
                "\n            .outer {display:inline-block;}\n            .inner {margin-top: 30px; color: black;}\n            .my-table {border-style: solid; border-color:black;border-width:1px;}\n            table.inner > thead {background-color: lightgray; color: black;}\n            .values {margin-left: 10px;}\n            .cheap {color:green}\n            .moderate {color:blue}\n            .expensive { color:red}\n        "
            ]
        }), 
        __metadata('design:paramtypes', [events_service_1_1.EventService])
    ], EventComponent);
    return EventComponent;
}());
exports.EventComponent = EventComponent;
//# sourceMappingURL=events.component.js.map