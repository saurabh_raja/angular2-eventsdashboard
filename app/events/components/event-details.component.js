"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var events_service_1_1 = require('../services/events.service.1');
var EventDetailsComponent = (function () {
    function EventDetailsComponent(_eventService, route) {
        this._eventService = _eventService;
        this.route = route;
        this.eventDetail = {};
        this.imageHeight = 100;
        this.imageWidth = 100;
    }
    EventDetailsComponent.prototype.ngOnInit = function () {
        this.getSingleEvent();
    };
    EventDetailsComponent.prototype.getSingleEvent = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
        });
        this.event = this._eventService.getEventDetails(this.id);
        this.title = "Details of Event - " + this.event.eventName;
    };
    EventDetailsComponent.prototype.getEventNameColor = function (fees) {
        var className = "";
        if (fees == 300) {
            className = "cheap";
        }
        else if (fees == 500) {
            className = "moderate";
        }
        else if (fees > 500) {
            className = "expensive";
        }
        return className;
    };
    EventDetailsComponent = __decorate([
        core_1.Component({
            selector: "event-details",
            templateUrl: "app/events/views/event-details.component.html",
            styles: [
                "\n            .bold { font-weight: bold }\n            .margin-top-3 { margin-top : 3% }\n        "
            ]
        }), 
        __metadata('design:paramtypes', [events_service_1_1.EventService, router_1.ActivatedRoute])
    ], EventDetailsComponent);
    return EventDetailsComponent;
}());
exports.EventDetailsComponent = EventDetailsComponent;
//# sourceMappingURL=event-details.component.js.map