"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var events_service_1_1 = require('../services/events.service.1');
var EventRegisterComponent = (function () {
    function EventRegisterComponent(_eventService, router) {
        this._eventService = _eventService;
        this.router = router;
    }
    EventRegisterComponent.prototype.ngOnInit = function () {
    };
    EventRegisterComponent.prototype.saveNewEvent = function (event) {
        console.log("First Event");
        console.log(event);
        this._eventService.saveEvent(event);
        this.router.navigate(['/events']);
    };
    EventRegisterComponent = __decorate([
        core_1.Component({
            selector: "event-newevent",
            templateUrl: "app/events/views/event-newevent.component.html",
            styles: [
                "\n            .margin-top-1 { margin-top:1% }\n            .error input { background-color: #E3C3C5 }\n            .font-red { color: red }\n        "
            ]
        }), 
        __metadata('design:paramtypes', [events_service_1_1.EventService, router_1.Router])
    ], EventRegisterComponent);
    return EventRegisterComponent;
}());
exports.EventRegisterComponent = EventRegisterComponent;
//# sourceMappingURL=event-newevent.component.js.map