import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Event } from "../models/events.model";

@Component({
    selector: "event-thumbnail",
    templateUrl:"app/events/views/event-thumbnail.component.html"
})

export class EventThumbnailComponent {
    @Input() event : Event;

    @Output() photoDetails : EventEmitter<string> = new EventEmitter<string>();
    checkedOut() {
        this.photoDetails.emit("Photo has been confirmed");
    }
}