import { Component, OnInit } from "@angular/core";
import { Event } from "../models/events.model";

import { EventThumbnailComponent } from "./event-thumbnail.component";

import { EventService } from '../services/events.service.1';

@Component({
    selector: "event-list",
    templateUrl:"app/events/views/events.component.html",
    styles: [
        `
            .outer {display:inline-block;}
            .inner {margin-top: 30px; color: black;}
            .my-table {border-style: solid; border-color:black;border-width:1px;}
            table.inner > thead {background-color: lightgray; color: black;}
            .values {margin-left: 10px;}
            .cheap {color:green}
            .moderate {color:blue}
            .expensive { color:red}
        `
    ]
})

export class EventComponent implements OnInit {
    constructor(private _eventService : EventService) {

    }

    ngOnInit() {
        this.events = this._eventService.getAllEvents();
    }

    title: string = "Event List";

    events: Event[];

    selectedEvent : Event;

    photoClickedMessage : string = "Not checked out";

    filterBy : string = "";

    getEventNameColor(fees : number) : String {
        let className = "";

        if(fees == 300) {
            className = "cheap";
        } else if(fees == 500) {
            className = "moderate";
        } else if(fees > 500) {
            className = "expensive";
        }

        return className;
    }
}
