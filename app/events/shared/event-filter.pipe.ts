import { Pipe, PipeTransform } from '@angular/core';

import { Event } from '../models/events.model'

@Pipe({
    name: 'eventFilter'
})

export class EventFilterPipe implements PipeTransform {
    transform(value : Event[], args : string[]) : Event[] {
        console.log("Wait : " + args);
        let filter : string = args[0] ? args[0].toLocaleLowerCase() : null;

        console.log(filter);

        return filter ? value.filter((event : Event) =>
            event.eventName[0].toLocaleLowerCase() == filter) : value;
    }
}