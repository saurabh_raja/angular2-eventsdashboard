import { Pipe, PipeTransform } from '@angular/core';

import { Event } from '../models/events.model'

@Pipe({
    name: 'eventOrderBy'
})

export class EventOrderByPipe implements PipeTransform {
    transform(value : Event[], args : string[]) : Event[] {
        let filter : string = args[0] ? args[0].toLocaleLowerCase() : null;

        return filter ? value.filter((event : Event) =>
            event.eventName[0].toLocaleLowerCase() == filter) : value;
    }
}