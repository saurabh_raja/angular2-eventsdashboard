import { Component, OnInit } from "@angular/core";

import { JsonPost } from "../models/json-post.model";

import { JsonPostService } from '../services/json-post.service';

@Component({
    selector: "jsonpost-list",
    templateUrl:"app/jsonpost/views/json-post.component.html",
    styles: [
        `
            
        `
    ]
})

export class JsonPostComponent implements OnInit {
    constructor(private _jsonPostService : JsonPostService) {

    }

    ngOnInit() {
        this.getAllPosts();
    }

    getAllPosts() {
        this._jsonPostService.getAllPosts().subscribe(data => this.posts = data, error => alert(error), () => console.log("Completed."));
    }

    showPostDetails(postId : number) {
        this._jsonPostService.getPostDetails(postId).subscribe(data => this.selectedPost = data, error => alert(error), () => console.log("Completed."));
    }

    title: string = "Json Posts List";

    posts: JsonPost[];

    selectedPost : JsonPost;

    filterBy : string = "";

    sortAsc() {
        this.posts.sort((e1,e2) => {
            if(e1.id > e2.id) return 1;
            if(e1.id === e2.id) return 0;
            if(e1.id < e2.id) return -1;
        })
    }

    sortDsc() {
        this.posts.sort((e1,e2) => {
            if(e1.id > e2.id) return -1;
            if(e1.id === e2.id) return 0;
            if(e1.id < e2.id) return 1;
        })
    }
}
