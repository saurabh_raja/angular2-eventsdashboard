"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var json_post_service_1 = require('../services/json-post.service');
var JsonPostComponent = (function () {
    function JsonPostComponent(_jsonPostService) {
        this._jsonPostService = _jsonPostService;
        this.title = "Json Posts List";
        this.filterBy = "";
    }
    JsonPostComponent.prototype.ngOnInit = function () {
        this.getAllPosts();
    };
    JsonPostComponent.prototype.getAllPosts = function () {
        var _this = this;
        this._jsonPostService.getAllPosts().subscribe(function (data) { return _this.posts = data; }, function (error) { return alert(error); }, function () { return console.log("Completed."); });
    };
    JsonPostComponent.prototype.showPostDetails = function (postId) {
        var _this = this;
        this._jsonPostService.getPostDetails(postId).subscribe(function (data) { return _this.selectedPost = data; }, function (error) { return alert(error); }, function () { return console.log("Completed."); });
    };
    JsonPostComponent.prototype.sortAsc = function () {
        this.posts.sort(function (e1, e2) {
            if (e1.id > e2.id)
                return 1;
            if (e1.id === e2.id)
                return 0;
            if (e1.id < e2.id)
                return -1;
        });
    };
    JsonPostComponent.prototype.sortDsc = function () {
        this.posts.sort(function (e1, e2) {
            if (e1.id > e2.id)
                return -1;
            if (e1.id === e2.id)
                return 0;
            if (e1.id < e2.id)
                return 1;
        });
    };
    JsonPostComponent = __decorate([
        core_1.Component({
            selector: "jsonpost-list",
            templateUrl: "app/jsonpost/views/json-post.component.html",
            styles: [
                "\n            \n        "
            ]
        }), 
        __metadata('design:paramtypes', [json_post_service_1.JsonPostService])
    ], JsonPostComponent);
    return JsonPostComponent;
}());
exports.JsonPostComponent = JsonPostComponent;
//# sourceMappingURL=json-post.component.js.map