import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { JsonPost } from '../models/json-post.model';

@Injectable()
export class JsonPostService {
    constructor(private _http : Http) {}

    private _jsonpostBaseUrl = "http://jsonplaceholder.typicode.com/posts";

    getAllPosts() : Observable<JsonPost[]> {
        return this._http.get(this._jsonpostBaseUrl)
            .map(res => res.json());    // Fat arrow inline function. json() returns stringify data
    }

    getPostDetails(postId : number) : Observable<JsonPost> {
        let url : string = this._jsonpostBaseUrl + '/' + postId;

        return this._http.get(url)
            .map(res => res.json());
    }
}
