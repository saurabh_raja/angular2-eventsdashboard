"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
var JsonPostService = (function () {
    function JsonPostService(_http) {
        this._http = _http;
        this._jsonpostBaseUrl = "http://jsonplaceholder.typicode.com/posts";
    }
    JsonPostService.prototype.getAllPosts = function () {
        return this._http.get(this._jsonpostBaseUrl)
            .map(function (res) { return res.json(); }); // Fat arrow inline function. json() returns stringify data
    };
    JsonPostService.prototype.getPostDetails = function (postId) {
        var url = this._jsonpostBaseUrl + '/' + postId;
        return this._http.get(url)
            .map(function (res) { return res.json(); });
    };
    JsonPostService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], JsonPostService);
    return JsonPostService;
}());
exports.JsonPostService = JsonPostService;
//# sourceMappingURL=json-post.service.js.map