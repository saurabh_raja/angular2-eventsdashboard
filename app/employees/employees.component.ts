import { Component } from "@angular/core";

import { Employee } from "./shared/employees.model";

@Component({
    selector: "employee-list",
    templateUrl: "app/employees/employees.component.html"
})

export class EmployeeComponent {
    title: string = "Employee List";

    employees: Employee[] = [
        {
            "employeeName": "Saurabh",
            "employeeId": "86681_FS",
            "employeeAge": 25
        },
        {
            "employeeName": "Abhishek",
            "employeeId": "86680_FS",
            "employeeAge": 25
        }
    ];
}