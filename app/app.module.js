"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var app_routing_1 = require('./app.routing');
var app_component_1 = require('./app.component');
var home_component_1 = require('./home/components/home.component');
var events_component_1 = require('./events/components/events.component');
var event_newevent_component_1 = require('./events/components/event-newevent.component');
var event_thumbnail_component_1 = require('./events/components/event-thumbnail.component');
var employees_component_1 = require('./employees/employees.component');
var json_post_component_1 = require('./jsonpost/components/json-post.component');
var event_details_component_1 = require('./events/components/event-details.component');
var first_letter_capital_pipe_1 = require('./events/shared/first-letter-capital.pipe');
var event_filter_pipe_1 = require('./events/shared/event-filter.pipe');
var events_service_1_1 = require('./events/services/events.service.1');
var json_post_service_1 = require('./jsonpost/services/json-post.service');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, app_routing_1.routing],
            declarations: [app_component_1.AppComponent, home_component_1.HomeComponent, events_component_1.EventComponent, event_details_component_1.EventDetailsComponent, event_newevent_component_1.EventRegisterComponent, employees_component_1.EmployeeComponent, event_thumbnail_component_1.EventThumbnailComponent, json_post_component_1.JsonPostComponent, first_letter_capital_pipe_1.FirstLetterCapitalPipe, event_filter_pipe_1.EventFilterPipe],
            bootstrap: [app_component_1.AppComponent],
            providers: [events_service_1_1.EventService, json_post_service_1.JsonPostService]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map