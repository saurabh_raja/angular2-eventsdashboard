"use strict";
var router_1 = require('@angular/router');
var home_component_1 = require('./home/components/home.component');
var events_component_1 = require('./events/components/events.component');
var event_details_component_1 = require('./events/components/event-details.component');
var event_newevent_component_1 = require('./events/components/event-newevent.component');
var routes = [
    {
        path: 'home',
        component: home_component_1.HomeComponent
    },
    {
        path: 'events',
        component: events_component_1.EventComponent
    },
    {
        path: 'eventdetails/:id',
        component: event_details_component_1.EventDetailsComponent
    },
    {
        path: 'eventregister',
        component: event_newevent_component_1.EventRegisterComponent
    },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full' // Utilize for query string
    }
];
exports.routing = router_1.RouterModule.forRoot(routes);
//# sourceMappingURL=app.routing.js.map