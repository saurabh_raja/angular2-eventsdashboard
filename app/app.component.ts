import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html'
})

export class AppComponent {
  title = "Future Events";
  subTitle = "Use this Boiler Plate for all the Angular Projects!";
}