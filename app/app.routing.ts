import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/components/home.component';
import { EventComponent } from './events/components/events.component';
import { EventDetailsComponent } from './events/components/event-details.component';
import { EventRegisterComponent } from './events/components/event-newevent.component';

const routes : Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'events',
        component: EventComponent
    },
    {
        path: 'eventdetails/:id',
        component: EventDetailsComponent
    },
    {
        path: 'eventregister',
        component: EventRegisterComponent
    },
    {
        path:'',
        redirectTo: '/home',
        pathMatch: 'full' // Utilize for query string
    }
];

export const routing : ModuleWithProviders = RouterModule.forRoot(routes);